QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TARGET = garuda-gamer
TEMPLATE = app
CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
# Build section
BUILD_PREFIX = $$(CA_BUILD_DIR)

isEmpty( BUILD_PREFIX ) {
        BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/moc-qt5
OBJECTS_DIR   = $$BUILD_PREFIX/obj-qt5
RCC_DIR	      = $$BUILD_PREFIX/qrc-qt5
UI_DIR        = $$BUILD_PREFIX/uic-qt5

# Disable QDebug on Release build
#CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

# Unused not working
#CONFIG(release):DEFINES += QT_NO_DEBUG_OUTPUT

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }
        BINDIR = $$PREFIX/bin

        target.path = $$BINDIR

        desktop.path = $$PREFIX/share/applications/
        desktop.files = "garuda-gamer.desktop"

        icons.path = /usr/share/icons/hicolor/scalable/apps/
        icons.files = garuda-gamer.svg

        SCRIPTS_PATH = $$PREFIX/share/garuda/

        scripts.files = scripts
        scripts.path  = $$SCRIPTS_PATH

        INSTALLS += target icons desktop scripts
}

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui
    
RESOURCES += \
    images.qrc

DISTFILES += \
    icons/DOSBox.png \
    icons/GameConqueror.png \
    icons/anbox.webp \
    icons/antimicrox.png \
    icons/athenaeum.png \
    icons/citra.png \
    icons/com.usebottles.bottles.svg \
    icons/corectrl.png \
    icons/desmume.png \
    icons/dgen.png \
    icons/displaycal.png \
    icons/dolphin-emu.png \
    icons/droidcam.png \
    icons/duckstation.png \
    icons/easyeffects.svg \
    icons/epsxe.png \
    icons/es-0.png \
    icons/es.ico \
    icons/fancontrol.svg \
    icons/fastgame.svg \
    icons/fceux.png \
    icons/fs-uae.png \
    icons/gambatte-logo-GBC-Emulator.webp \
    icons/gamehub.svg \
    icons/gamerworld.png \
    icons/gensgs_48x48.png \
    icons/goverlay.png \
    icons/guiscrcpy_logo.png \
    icons/gwe.png \
    icons/hatari.png \
    icons/heroic-icon.png \
    icons/higan.png \
    icons/icon-scalable.svg \
    icons/itch.svg \
    icons/kega-fusion.png \
    icons/keyboarvisualizer.png \
    icons/legendary.png \
    icons/librewish.png \
    icons/lutris.png \
    icons/mame.svg \
    icons/mednaffe.png \
    icons/mgba.png \
    icons/minigalaxy.png \
    icons/moonlight.png \
    icons/mumble.svg \
    icons/mupen64plus.png \
    icons/nestopia.png \
    icons/noisetorch.png \
    icons/nyrna.png \
    icons/openrgb.png \
    icons/oversteer.svg \
    icons/pcsx2.png \
    icons/pcsxr.png \
    icons/pegasus.png \
    icons/piper.svg \
    icons/playit.png \
    icons/ppsspp.png \
    icons/proton.svg \
    icons/qemu_64x64.png \
    icons/reicast.png \
    icons/retroarch.png \
    icons/rpcs3.png \
    icons/sc-controller.svg \
    icons/scummvm.png \
    icons/snes9x_64x64.png \
    icons/soundwire.jpg \
    icons/steam.png \
    icons/stella-64x64.png \
    icons/vbam.svg \
    icons/winehq.png \
    icons/xemu_64x64.png \
    icons/yabause.png \
    icons/yuzu.svg \
    icons/zsnes.png


