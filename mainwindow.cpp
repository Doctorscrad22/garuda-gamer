﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    updateCheckBoxes();

    // Set up callback for installer process finish
    connect(&installer, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(on_installerFinished(int, QProcess::ExitStatus)));
}

MainWindow::~MainWindow() { delete ui; }

QList<QCheckBox*> MainWindow::getCheckboxes()
{
    return this->ui->tabWidget->findChildren<QCheckBox*>();
}

void MainWindow::updateCheckBoxes()
{
    updatePackageList();
    for (auto element : getCheckboxes()) {
        auto string = element->property("packageName").toString();
        element->setChecked(installed_packages.contains(string));
    }
}

void MainWindow::updatePackageList()
{
    QStringList packages;

    for (auto element : getCheckboxes()) {
        auto string = element->property("packageName").toString();
        if (string.isEmpty())
            std::runtime_error(("Empty/Non existent packageName field for " + element->objectName()).toStdString());
        packages.append(std::move(string));
    }

    QProcess proc;
    proc.start("pacman -Qq " + packages.join(" "));
    proc.waitForFinished();
    QString installed = proc.readAllStandardOutput();
    installed_packages = installed.split("\n");
}

void MainWindow::lockCheckboxes(bool locked)
{
    for (auto element : getCheckboxes()) {
        // We can't use setEnabled here since it scrolls up to the top for some reason
        element->setAttribute(Qt::WA_TransparentForMouseEvents, locked);
        element->setFocusPolicy(locked ? Qt::NoFocus : Qt::StrongFocus);
    }

    this->ui->applyButtons->setAttribute(Qt::WA_TransparentForMouseEvents, locked);
    this->ui->applyButtons->setFocusPolicy(locked ? Qt::NoFocus : Qt::StrongFocus);
}

void MainWindow::on_installerFinished(int, QProcess::ExitStatus)
{
    if (!allowUnlock)
        return;
    updateCheckBoxes();
    lockCheckboxes(false);
}

void MainWindow::on_applyButtons_clicked(QAbstractButton* button)
{
    if (installer.state() != installer.NotRunning)
        return;

    if (ui->applyButtons->buttonRole(button) == ui->applyButtons->ApplyRole) {
        updatePackageList();

        QStringList removed_packages;
        QStringList added_packages;

        for (auto element : getCheckboxes()) {
            auto packagename = element->property("packageName").toString();
            auto checked = element->isChecked();

            if (installed_packages.contains(packagename)) {
                if (!checked)
                    removed_packages.append(std::move(packagename));
            } else {
                if (checked)
                    added_packages.append(std::move(packagename));
            }
        }

        if (added_packages.empty() && removed_packages.empty()) {
            QMessageBox::warning(this, "Garuda Gamer", "The current configuration is already applied on your system.");
            return;
        }

        allowUnlock = false;
        lockCheckboxes(true);

        QString cmd;
        if (!added_packages.empty())
            cmd.append("pacman -S --noconfirm " + added_packages.join(" ") + ";");
        if (!removed_packages.empty())
            cmd.append("pacman -Rns --noconfirm " + removed_packages.join(" ") + ";");
        cmd += "read -p 'Press enter to exit'";

        auto paramlist = QStringList();
        paramlist << "-s" << "pkexec /usr/share/garuda/scripts/garuda-gamer/rootshell.sh";
        paramlist << cmd;
        allowUnlock = true;
        installer.start("/usr/lib/garuda/launch-terminal", paramlist);
    } else
        updateCheckBoxes();
}
