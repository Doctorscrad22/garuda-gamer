#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QStringList>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
class QCheckBox;
class QAbstractButton;
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

private:
    QStringList installed_packages;
    QProcess installer;
    bool allowUnlock;

    QList<QCheckBox*> getCheckboxes();
    void updateCheckBoxes();
    void lockCheckboxes(bool locked);
    void updatePackageList();

private slots:
    void on_installerFinished(int, QProcess::ExitStatus);
    void on_applyButtons_clicked(QAbstractButton* button);

private:
    Ui::MainWindow* ui;
};
#endif // MAINWINDOW_H
